{
    'name' : 'quotation_proforma',
    'version' : '0.1',
    'author' : 'Mithril Informatique',
    'sequence': 130,
    'category': '',
    'website' : 'https://www.mithril.re',
    'summary' : '',
    'description' : "",
    'depends' : [
        'base',
        'sale',
    ],
    'data' : [
    		'quotation_proforma_view.xml',
    ],

    'installable' : True,
    'application' : False,
}
